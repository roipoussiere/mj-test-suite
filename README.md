# Majority Judgment test suite

*A test suite for majority judgment implementations.*

To be checked against this test suite, your majority judgment application should
be able to take the election counting as input and return the election merit profile and result.

This test suite provides a couple of input and output files for a set of election samples:
when running your tests, you must gives to your application each input file,
and compare the result with the corresponding output file.

These input and output data must follow a specification described bellow.

## Input

Input data are stored in `.mjc.yaml` files (`mjc` for Majority Judgment Counting).

They describe the raw data of an election counting and respect the following syntax:

```yaml
_question: Which voting system should be used?
_proposals: [proposal 1, proposal 2, proposal 3, proposal 4]
_mentions: [reject, insufficient, poor, fair, good, very good, excellent]
bege: 1
abea: 2
eecg: 2
fccb: 1
```

## Output

Output data are stored in `.mjr.yaml` files (`mjr` for Majority Judgment Result).

They describe the merite profile of the election, among with the final result, and respect the following syntax:

```yaml
question: Which voting system should be used?
mentions: [reject, insufficient, poor, fair, good, very good, excellent]
proposals:
  - name: proposal 1
    rank: 1
    tally: [2, 1, 0, 0, 2, 1, 0]
  - name: proposal 2
    rank: 2
    tally: [0, 1, 1, 0, 2, 0, 0]
  - name: proposal 3
    rank: 3
    tally: [0, 0, 2, 0, 1, 0, 2]
  - name: proposal 4
    rank: 4
    tally: [1, 1, 0, 0, 1, 0, 1]
```
